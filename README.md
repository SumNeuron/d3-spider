# d3-spider
A shameless generalization of the radar plot from [Nadieh Bremer](http://bl.ocks.org/nbremer/21746a9668ffdf6d8242).


# Install

```
npm i d3-spider
```

# Usage

```js
import {spider} from 'd3-spider'
```

## Assumptions

It is assumed that the `data` bound to `spider` is an object, where each key
corresponds to a collection of all the values of the "web":

```js
data = {
  A: {
    x: 1,
    y: 2,
    z: 3
  },
  B:  {
    x: 3,
    y: 1,
    z: 3
  }
}
```

more complex data can be provided should utility functions such as `valueExtractor`
be overwritten from their default behavior:

```js
data = {
  A: {
    x: { value: 1 },
    y: { value: 2 },
    ...
  },
  ...
}
