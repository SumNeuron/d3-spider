import { keys, curveCardinalClosed, scaleSequential, interpolateViridis, scaleLinear, easeSin, extent, range, lineRadial, select } from 'd3';

if (typeof document !== "undefined") {
  var element = document.documentElement;
}

function getTranslation(transform) {
  var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
  transform = transform == undefined ? 'translate(0,0)' : transform;
  g.setAttributeNS(null, 'transform', transform);
  var matrix = g.transform.baseVal.consolidate().matrix;
  return [matrix.e, matrix.f];
}
function resizeDebounce(f, wait) {
  var resize = debounce(function () {
    f();
  }, wait);
  window.addEventListener('resize', resize);
}
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
        args = arguments;

    var later = function later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}
function wrapText(text, width) {
  text.each(function () {
    var text = select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.4,
        // ems
    y = text.attr("y"),
        x = text.attr("x"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");

    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));

      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}

var utils = /*#__PURE__*/Object.freeze({
  getTranslation: getTranslation,
  resizeDebounce: resizeDebounce,
  debounce: debounce,
  wrapText: wrapText
});

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function spider(container) {
  var data,
      dataKeys,
      axes,
      dataAxes,
      axesExtractor = function axesExtractor(k, i) {
    return keys(data[k]);
  },
      values,
      dataValues,
      valueExtractor = function valueExtractor(k, i, a, j) {
    var v = data[k][a];
    if (v === undefined) return 0;
    return v;
  },
      spaceX,
      spaceY,
      spaceM,
      // minimum space
  radius,
      angleR,
      // angle in raidans
  dataExtent,
      numberOfLevels = 5,
      curve = curveCardinalClosed,
      colorExtractor = function colorExtractor(key, index) {
    return scaleSequential().interpolator(interpolateViridis)(index / dataKeys.length);
  },
      wrapWidth = 60,
      scaleR = scaleLinear(),
      namespace = 'spider',
      n = function n(s) {
    return "".concat(namespace, "-").concat(s);
  },
      levelFill = '#CDCDCD',
      levelStroke = '#CDCDCD',
      levelOpacity = 0.1,
      levelBlur = true,
      levelLabelFontSize = '10px',
      levelLabelFill = '#737373',
      spokeStroke = 'white',
      spokeStrokeWidth = '2px',
      spokeFontSize = '11px',
      spokeLabelSpacer = 10,
      spokeExtension = 5,
      radiusPercent = 0.95,
      webStrokeWidth = 0.9,
      webOpacity = 0.3,
      webPointRadius = 4,
      webPointOpacity = 0.8,
      webPointsContainer,
      webPoints,
      webFills,
      webOutlines,
      webs,
      webContainer,
      spiderLine,
      spokeLabels,
      spokeLines,
      axesSpokes,
      axesLabels,
      axesLevels,
      webPointMouseover = function webPointMouseover(k, i) {},
      webPointMouseleave = function webPointMouseleave(k, i) {},
      webFillMouseover = function webFillMouseover(k, i) {},
      webFillMouseleave = function webFillMouseleave(k, i) {},
      spokeLabelMouseover = function spokeLabelMouseover(k, i) {},
      spokeLabelMouseleave = function spokeLabelMouseleave(k, i) {},
      transitionDuration = 500,
      easeFn = easeSin;

  spider.data = function (_) {
    return arguments.length ? (data = _, spider) : data;
  };

  spider.dataKeys = function (_) {
    return arguments.length ? (dataKeys = _, spider) : dataKeys;
  };

  spider.axes = function (_) {
    return arguments.length ? (axes = _, spider) : axes;
  };

  spider.dataAxes = function (_) {
    return arguments.length ? (dataAxes = _, spider) : dataAxes;
  };

  spider.axesExtractor = function (_) {
    return arguments.length ? (axesExtractor = _, spider) : axesExtractor;
  };

  spider.dataValues = function (_) {
    return arguments.length ? (dataValues = _, spider) : dataValues;
  };

  spider.valueExtractor = function (_) {
    return arguments.length ? (valueExtractor = _, spider) : valueExtractor;
  };

  spider.spaceX = function (_) {
    return arguments.length ? (spaceX = _, spider) : spaceX;
  };

  spider.spaceY = function (_) {
    return arguments.length ? (spaceY = _, spider) : spaceY;
  };

  spider.spaceM = function (_) {
    return arguments.length ? (spaceM = _, spider) : spaceM;
  };

  spider.radius = function (_) {
    return arguments.length ? (radius = _, spider) : radius;
  };

  spider.angleR = function (_) {
    return arguments.length ? (angleR = _, spider) : angleR;
  };

  spider.dataExtent = function (_) {
    return arguments.length ? (dataExtent = _, spider) : dataExtent;
  };

  spider.numberOfLevels = function (_) {
    return arguments.length ? (numberOfLevels = _, spider) : numberOfLevels;
  };

  spider.curve = function (_) {
    return arguments.length ? (curve = _, spider) : curve;
  };

  spider.colorExtractor = function (_) {
    return arguments.length ? (colorExtractor = _, spider) : colorExtractor;
  };

  spider.wrapWidth = function (_) {
    return arguments.length ? (wrapWidth = _, spider) : wrapWidth;
  };

  spider.scaleR = function (_) {
    return arguments.length ? (scaleR = _, spider) : scaleR;
  };

  spider.namespace = function (_) {
    return arguments.length ? (namespace = _, spider) : namespace;
  };

  spider.levelFill = function (_) {
    return arguments.length ? (levelFill = _, spider) : levelFill;
  };

  spider.levelStroke = function (_) {
    return arguments.length ? (levelStroke = _, spider) : levelStroke;
  };

  spider.levelOpacity = function (_) {
    return arguments.length ? (levelOpacity = _, spider) : levelOpacity;
  };

  spider.levelBlur = function (_) {
    return arguments.length ? (levelBlur = _, spider) : levelBlur;
  };

  spider.levelLabelFontSize = function (_) {
    return arguments.length ? (levelLabelFontSize = _, spider) : levelLabelFontSize;
  };

  spider.levelLabelFill = function (_) {
    return arguments.length ? (levelLabelFill = _, spider) : levelLabelFill;
  };

  spider.spokeStroke = function (_) {
    return arguments.length ? (spokeStroke = _, spider) : spokeStroke;
  };

  spider.spokeStrokeWidth = function (_) {
    return arguments.length ? (spokeStrokeWidth = _, spider) : spokeStrokeWidth;
  };

  spider.spokeFontSize = function (_) {
    return arguments.length ? (spokeFontSize = _, spider) : spokeFontSize;
  };

  spider.spokeLabelSpacer = function (_) {
    return arguments.length ? (spokeLabelSpacer = _, spider) : spokeLabelSpacer;
  };

  spider.spokeExtension = function (_) {
    return arguments.length ? (spokeExtension = _, spider) : spokeExtension;
  };

  spider.radiusPercent = function (_) {
    return arguments.length ? (radiusPercent = _, spider) : radiusPercent;
  };

  spider.webStrokeWidth = function (_) {
    return arguments.length ? (webStrokeWidth = _, spider) : webStrokeWidth;
  };

  spider.webOpacity = function (_) {
    return arguments.length ? (webOpacity = _, spider) : webOpacity;
  };

  spider.webPointRadius = function (_) {
    return arguments.length ? (webPointRadius = _, spider) : webPointRadius;
  };

  spider.webPointOpacity = function (_) {
    return arguments.length ? (webPointOpacity = _, spider) : webPointOpacity;
  };

  spider.webPointsContainer = function (_) {
    return arguments.length ? (webPointsContainer = _, spider) : webPointsContainer;
  };

  spider.webPoints = function (_) {
    return arguments.length ? (webPoints = _, spider) : webPoints;
  };

  spider.webFills = function (_) {
    return arguments.length ? (webFills = _, spider) : webFills;
  };

  spider.webOutlines = function (_) {
    return arguments.length ? (webOutlines = _, spider) : webOutlines;
  };

  spider.webs = function (_) {
    return arguments.length ? (webs = _, spider) : webs;
  };

  spider.webContainer = function (_) {
    return arguments.length ? (webContainer = _, spider) : webContainer;
  };

  spider.spiderLine = function (_) {
    return arguments.length ? (spiderLine = _, spider) : spiderLine;
  };

  spider.spokeLabels = function (_) {
    return arguments.length ? (spokeLabels = _, spider) : spokeLabels;
  };

  spider.spokeLines = function (_) {
    return arguments.length ? (spokeLines = _, spider) : spokeLines;
  };

  spider.axesSpokes = function (_) {
    return arguments.length ? (axesSpokes = _, spider) : axesSpokes;
  };

  spider.axesLabels = function (_) {
    return arguments.length ? (axesLabels = _, spider) : axesLabels;
  };

  spider.axesLevels = function (_) {
    return arguments.length ? (axesLevels = _, spider) : axesLevels;
  };

  spider.webPointMouseover = function (_) {
    return arguments.length ? (webPointMouseover = _, spider) : webPointMouseover;
  };

  spider.webPointMouseleave = function (_) {
    return arguments.length ? (webPointMouseleave = _, spider) : webPointMouseleave;
  };

  spider.webFillMouseover = function (_) {
    return arguments.length ? (webFillMouseover = _, spider) : webFillMouseover;
  };

  spider.webFillMouseleave = function (_) {
    return arguments.length ? (webFillMouseleave = _, spider) : webFillMouseleave;
  };

  spider.spokeLabelMouseover = function (_) {
    return arguments.length ? (spokeLabelMouseover = _, spider) : spokeLabelMouseover;
  };

  spider.spokeLabelMouseleave = function (_) {
    return arguments.length ? (spokeLabelMouseleave = _, spider) : spokeLabelMouseleave;
  };

  spider.transitionDuration = function (_) {
    return arguments.length ? (transitionDuration = _, spider) : transitionDuration;
  };

  spider.easeFn = function (_) {
    return arguments.length ? (easeFn = _, spider) : easeFn;
  };

  function spider() {
    dataKeys = keys(data); // all unique axes

    axes = []; // axes which appear in each DS

    dataAxes = dataKeys.map(function (k, i) {
      var keys$$1 = axesExtractor(k, i);
      axes = axes.concat(keys$$1);
      return keys$$1;
    });
    axes = Array.from(new Set(axes));
    values = [];
    dataValues = dataKeys.map(function (k, i) {
      var relevantAxes = dataAxes[i];
      var vals = axes.map(function (a, j) {
        if (relevantAxes.indexOf(a) > -1) return valueExtractor(k, i, a, j);
        return 0;
      });
      values = values.concat(vals);
      return vals;
    });
    dataExtent = extent(values);

    var _dataExtent = dataExtent,
        _dataExtent2 = _slicedToArray(_dataExtent, 2),
        dataMin = _dataExtent2[0],
        dataMax = _dataExtent2[1];

    spaceM = Math.min(spaceX, spaceY);
    radius = spaceM / 2 * radiusPercent - (spokeLabelSpacer + spokeExtension) * 2;
    angleR = Math.PI * 2 / axes.length;
    scaleR.domain([0, dataMax]).range([0, radius]);
    var gCenter = container.select("g.".concat(n('center')));
    if (gCenter.empty()) gCenter = container.append('g').attr('class', "".concat(n('center')));
    gCenter.attr('transform', "translate(\n      ".concat((spaceX - (spokeLabelSpacer + spokeExtension * 2)) / 2, ",\n      ").concat((spaceY - (spokeLabelSpacer + spokeExtension * 2)) / 2, ")"));
    var defs = gCenter.select("defs");
    if (defs.empty()) defs = gCenter.append('defs');
    var filt = defs.select("filter#".concat(n('glow')));

    if (filt.empty()) {
      filt = defs.append('filter').attr('id', "".concat(n('glow')));
      var feGaussianBlur = filt.append('feGaussianBlur').attr('stdDeviation', '2.5').attr('result', 'coloredBlur'),
          feMerge = filt.append('feMerge'),
          feMergeNode_1 = feMerge.append('feMergeNode').attr('in', 'coloredBlur'),
          feMergeNode_2 = feMerge.append('feMergeNode').attr('in', 'SourceGraphic');
    }

    var gAxesCon = gCenter.select("g.".concat(n('axes-container')));
    if (gAxesCon.empty()) gAxesCon = gCenter.append('g').attr('class', "".concat(n('axes-container'))); // concentric numberOfLevels

    axesLevels = gAxesCon.selectAll(".".concat(n('axis-level'))).data(range(1, numberOfLevels + 1).reverse());
    axesLevels.exit().remove();
    axesLevels = axesLevels.merge(axesLevels.enter().append('circle').attr('class', "".concat(n('axis-level'))));
    axesLevels.transition(transitionDuration).ease(easeFn).attr('r', function (d, i) {
      return radius / numberOfLevels * d;
    }).style('fill', levelFill).style('stroke', levelStroke).attr('fill-opacity', levelOpacity);
    if (levelBlur) axesLevels.style('filter', "url(#".concat(n('glow'), ")")); // values of concentric numberOfLevels

    axesLabels = gAxesCon.selectAll(".".concat(n('axis-level-label'))).data(range(1, numberOfLevels + 1).reverse());
    axesLabels.exit().remove();
    axesLabels = axesLabels.merge(axesLabels.enter().append('text').attr('class', "".concat(n('axis-level-label'))));
    axesLabels.transition(transitionDuration).ease(easeFn).attr('x', 4).attr('y', function (d) {
      return -d * radius / numberOfLevels;
    }).attr('dy', '0.4em').style('font-size', levelLabelFontSize).attr('fill', levelLabelFill).text(function (d, i) {
      return dataMax * d / numberOfLevels;
    }); // spokes

    axesSpokes = gAxesCon.selectAll("g.".concat(n('axis-spoke'))).data(axes);
    axesSpokes.exit().remove();
    axesSpokes = axesSpokes.merge(axesSpokes.enter().append('g').attr('class', "".concat(n('axis-spoke'))));
    spokeLines = axesSpokes.select("line.".concat(n('axis-line')));
    if (spokeLines.empty()) spokeLines = axesSpokes.append('line').attr('class', "".concat(n('axis-line')));
    spokeLines.transition(transitionDuration).ease(easeFn).attr('x1', 0).attr('y1', 0).attr('x2', function (d, i) {
      return (scaleR(dataMax) + spokeExtension) * Math.cos(angleR * i - Math.PI / 2);
    }).attr('y2', function (d, i) {
      return (scaleR(dataMax) + spokeExtension) * Math.sin(angleR * i - Math.PI / 2);
    }).style('stroke', spokeStroke).style('stroke-width', spokeStrokeWidth);
    spokeLabels = axesSpokes.select("text.".concat(n('spoke-label')));
    if (spokeLabels.empty()) spokeLabels = axesSpokes.append('text').attr('class', "".concat(n('spoke-label')));
    spokeLabels.transition(transitionDuration).ease(easeFn).style('font-size', spokeFontSize).attr('text-anchor', 'middle').attr('dy', '0.35em').attr('x', function (d, i) {
      return (scaleR(dataMax) + spokeExtension + spokeLabelSpacer) * Math.cos(angleR * i - Math.PI / 2);
    }).attr('y', function (d, i) {
      return (scaleR(dataMax) + spokeExtension + spokeLabelSpacer) * Math.sin(angleR * i - Math.PI / 2);
    }).text(function (d) {
      return d;
    }).call(wrapText, wrapWidth);
    spokeLabels.on('mouseover', spokeLabelMouseover).on('mouseenter', spokeLabelMouseover).on('mouseleave', spokeLabelMouseleave).on('mouseexit', spokeLabelMouseleave);
    spiderLine = lineRadial().radius(function (d) {
      return scaleR(d);
    }).angle(function (d, i) {
      return angleR * i;
    }).curve(curve);
    var webContainer = gCenter.select("g.".concat(n('web-container')));
    if (webContainer.empty()) webContainer = gCenter.append('g').attr('class', "".concat(n('web-container')));
    webs = webContainer.selectAll("g.".concat(n('web-wrapper'))).data(dataKeys);
    webs.exit().remove();
    webs = webs.merge(webs.enter().append('g').attr('class', "".concat(n('web-wrapper'))));
    webOutlines = webs.select("path.".concat(n('web-outline')));
    if (webOutlines.empty()) webOutlines = webs.append('path').attr('class', "".concat(n('web-outline')));
    webOutlines.transition(transitionDuration).ease(easeFn).attr('d', function (d, i) {
      return spiderLine(dataValues[i]);
    }).attr('stroke', function (d, i) {
      return colorExtractor(d, i);
    }).attr('stroke-width', webStrokeWidth).style('fill', 'none');
    webFills = webs.select("path.".concat(n('web-fill')));
    if (webFills.empty()) webFills = webs.append('path').attr('class', "".concat(n('web-fill')));
    webFills.transition(transitionDuration).ease(easeFn).attr('d', function (d, i) {
      return spiderLine(dataValues[i]);
    }).attr('fill', function (d, i) {
      return colorExtractor(d, i);
    }).attr('opacity', webOpacity);
    webFills.on('mouseover', webFillMouseover).on('mouseenter', webFillMouseover).on('mouseleave', webFillMouseleave).on('mouseexit', webFillMouseleave);
    webPointsContainer = webs.select("g.".concat(n('web-point-con')));
    if (webPointsContainer.empty()) webPointsContainer = webs.append('g').attr('class', "".concat(n('web-point-con')));
    webPoints = webPointsContainer.selectAll("circle.".concat(n('web-point'))).data(function (d, i) {
      return dataValues[i].map(function (v, j) {
        return {
          v: v,
          d: d,
          a: axes[j]
        };
      });
    });
    webPoints.exit().remove();
    webPoints = webPoints.merge(webPoints.enter().append('circle').attr('class', "".concat(n('web-point'))));
    webPoints.transition(transitionDuration).ease(easeFn).attr('r', webPointRadius).attr('cx', function (d, i) {
      return scaleR(d.v) * Math.cos(angleR * i - Math.PI / 2);
    }).attr('cy', function (d, i) {
      return scaleR(d.v) * Math.sin(angleR * i - Math.PI / 2);
    }).style('fill', function (d, i, j) {
      return colorExtractor(d.d, dataKeys.indexOf(d.d));
    }).style('fill-opacity', webPointOpacity);
    webPoints.on('mouseover', webPointMouseover).on('mouseenter', webPointMouseover).on('mouseleave', webPointMouseleave).on('mouseexit', webPointMouseleave);
  }

  return spider;
}

var d3_spider = {
  spider: spider,
  utils: utils
};

if (typeof window !== 'undefined') {
  window.d3_spider = d3_spider;
}

export default d3_spider;
export { spider, utils };
