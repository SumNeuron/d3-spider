import { event, mouse} from 'd3-selection';

import spider from './modules/spider'
import * as utils from './modules/utils'


const d3_spider = {
  spider, utils
}

if (typeof window !== 'undefined') {
  window.d3_spider = d3_spider;
}


export default d3_spider
export {spider, utils}
