import * as d3 from 'd3'
import {wrapText} from './utils'

export default function spider(container) {
  let
  data,
  dataKeys,
  axes,
  dataAxes,
  axesExtractor = (k, i) => d3.keys(data[k]),
  values,
  dataValues,
  valueExtractor = (k, i, a, j) => {
    let v = data[k][a]
    if (v === undefined) return 0
    return v
  },
  spaceX,
  spaceY,
  spaceM, // minimum space
  radius,
  angleR, // angle in raidans
  dataExtent,
  numberOfLevels=5,
  curve=d3.curveCardinalClosed,
  colorExtractor = (key, index) => {
    return d3.scaleSequential()
    .interpolator(d3.interpolateViridis)
    (index / dataKeys.length)
  },
  wrapWidth=60,
  scaleR = d3.scaleLinear(),
  namespace='spider',
  n = (s) => `${namespace}-${s}`,


  levelFill='#CDCDCD',
  levelStroke='#CDCDCD',
  levelOpacity=0.1,
  levelBlur=true,
  levelLabelFontSize='10px',
  levelLabelFill = '#737373',
  spokeStroke = 'white',

  spokeStrokeWidth = '2px',
  spokeFontSize = '11px',
  spokeLabelSpacer = 10,
  spokeExtension=5,
  radiusPercent=0.95,
  webStrokeWidth = 0.9,
  webOpacity = 0.3,

  webPointRadius = 4,
  webPointOpacity = 0.8,
  webPointsContainer,
  webPoints,
  webFills,
  webOutlines,
  webs,

  webContainer,
  spiderLine,
  spokeLabels,
  spokeLines,
  axesSpokes,
  axesLabels,
  axesLevels,

  webPointMouseover = (k, i) => {},
  webPointMouseleave = (k, i) => {},
  webFillMouseover = (k, i) => {},
  webFillMouseleave = (k, i) => {},
  spokeLabelMouseover = (k, i) => {},
  spokeLabelMouseleave = (k, i) => {},

  transitionDuration = 500,
  easeFn = d3.easeSin


  spider.data = function(_) { return arguments.length ? (data = _, spider) : data; };
  spider.dataKeys = function(_) { return arguments.length ? (dataKeys = _, spider) : dataKeys; };
  spider.axes = function(_) { return arguments.length ? (axes = _, spider) : axes; };
  spider.dataAxes = function(_) { return arguments.length ? (dataAxes = _, spider) : dataAxes; };
  spider.axesExtractor = function(_) { return arguments.length ? (axesExtractor = _, spider) : axesExtractor; };
  spider.dataValues = function(_) { return arguments.length ? (dataValues = _, spider) : dataValues; };
  spider.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, spider) : valueExtractor; };
  spider.spaceX = function(_) { return arguments.length ? (spaceX = _, spider) : spaceX; };
  spider.spaceY = function(_) { return arguments.length ? (spaceY = _, spider) : spaceY; };
  spider.spaceM = function(_) { return arguments.length ? (spaceM = _, spider) : spaceM; };
  spider.radius = function(_) { return arguments.length ? (radius = _, spider) : radius; };
  spider.angleR = function(_) { return arguments.length ? (angleR = _, spider) : angleR; };
  spider.dataExtent = function(_) { return arguments.length ? (dataExtent = _, spider) : dataExtent; };
  spider.numberOfLevels = function(_) { return arguments.length ? (numberOfLevels = _, spider) : numberOfLevels; };
  spider.curve = function(_) { return arguments.length ? (curve = _, spider) : curve; };
  spider.colorExtractor = function(_) { return arguments.length ? (colorExtractor = _, spider) : colorExtractor; };
  spider.wrapWidth = function(_) { return arguments.length ? (wrapWidth = _, spider) : wrapWidth; };
  spider.scaleR = function(_) { return arguments.length ? (scaleR = _, spider) : scaleR; };
  spider.namespace = function(_) { return arguments.length ? (namespace = _, spider) : namespace; };



  spider.levelFill = function(_) { return arguments.length ? (levelFill = _, spider) : levelFill; };
  spider.levelStroke = function(_) { return arguments.length ? (levelStroke = _, spider) : levelStroke; };
  spider.levelOpacity = function(_) { return arguments.length ? (levelOpacity = _, spider) : levelOpacity; };
  spider.levelBlur = function(_) { return arguments.length ? (levelBlur = _, spider) : levelBlur; };
  spider.levelLabelFontSize = function(_) { return arguments.length ? (levelLabelFontSize = _, spider) : levelLabelFontSize; };
  spider.levelLabelFill = function(_) { return arguments.length ? (levelLabelFill = _, spider) : levelLabelFill; };
  spider.spokeStroke = function(_) { return arguments.length ? (spokeStroke = _, spider) : spokeStroke; };

  spider.spokeStrokeWidth = function(_) { return arguments.length ? (spokeStrokeWidth = _, spider) : spokeStrokeWidth; };
  spider.spokeFontSize = function(_) { return arguments.length ? (spokeFontSize = _, spider) : spokeFontSize; };
  spider.spokeLabelSpacer = function(_) { return arguments.length ? (spokeLabelSpacer = _, spider) : spokeLabelSpacer; };
  spider.spokeExtension = function(_) { return arguments.length ? (spokeExtension = _, spider) : spokeExtension; };
  spider.radiusPercent = function(_) { return arguments.length ? (radiusPercent = _, spider) : radiusPercent; };
  spider.webStrokeWidth = function(_) { return arguments.length ? (webStrokeWidth = _, spider) : webStrokeWidth; };
  spider.webOpacity = function(_) { return arguments.length ? (webOpacity = _, spider) : webOpacity; };

  spider.webPointRadius = function(_) { return arguments.length ? (webPointRadius = _, spider) : webPointRadius; };
  spider.webPointOpacity = function(_) { return arguments.length ? (webPointOpacity = _, spider) : webPointOpacity; };
  spider.webPointsContainer = function(_) { return arguments.length ? (webPointsContainer = _, spider) : webPointsContainer; };
  spider.webPoints = function(_) { return arguments.length ? (webPoints = _, spider) : webPoints; };
  spider.webFills = function(_) { return arguments.length ? (webFills = _, spider) : webFills; };
  spider.webOutlines = function(_) { return arguments.length ? (webOutlines = _, spider) : webOutlines; };
  spider.webs = function(_) { return arguments.length ? (webs = _, spider) : webs; };

  spider.webContainer = function(_) { return arguments.length ? (webContainer = _, spider) : webContainer; };
  spider.spiderLine = function(_) { return arguments.length ? (spiderLine = _, spider) : spiderLine; };
  spider.spokeLabels = function(_) { return arguments.length ? (spokeLabels = _, spider) : spokeLabels; };
  spider.spokeLines = function(_) { return arguments.length ? (spokeLines = _, spider) : spokeLines; };
  spider.axesSpokes = function(_) { return arguments.length ? (axesSpokes = _, spider) : axesSpokes; };
  spider.axesLabels = function(_) { return arguments.length ? (axesLabels = _, spider) : axesLabels; };
  spider.axesLevels = function(_) { return arguments.length ? (axesLevels = _, spider) : axesLevels; };

  spider.webPointMouseover = function(_) { return arguments.length ? (webPointMouseover = _, spider) : webPointMouseover; };
  spider.webPointMouseleave = function(_) { return arguments.length ? (webPointMouseleave = _, spider) : webPointMouseleave; };
  spider.webFillMouseover = function(_) { return arguments.length ? (webFillMouseover = _, spider) : webFillMouseover; };
  spider.webFillMouseleave = function(_) { return arguments.length ? (webFillMouseleave = _, spider) : webFillMouseleave; };
  spider.spokeLabelMouseover = function(_) { return arguments.length ? (spokeLabelMouseover = _, spider) : spokeLabelMouseover; };
  spider.spokeLabelMouseleave = function(_) { return arguments.length ? (spokeLabelMouseleave = _, spider) : spokeLabelMouseleave; };

  spider.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, spider) : transitionDuration; };
  spider.easeFn = function(_) { return arguments.length ? (easeFn = _, spider) : easeFn; };











  function spider() {





    dataKeys = d3.keys(data)
    // all unique axes
    axes = []
    // axes which appear in each DS
    dataAxes = dataKeys.map((k,i)=>{
      let keys = axesExtractor(k, i)
      axes = axes.concat(keys)
      return keys
    })
    axes = Array.from(new Set(axes))

    values = []
    dataValues = dataKeys.map((k, i)=>{
      let relevantAxes = dataAxes[i]
      let vals = axes.map((a, j)=>{
        if (relevantAxes.indexOf(a) > -1) return valueExtractor(k,i,a,j)
        return 0
      })
      values = values.concat(vals)
      return vals
    })


    dataExtent = d3.extent(values)
    let [dataMin, dataMax] = dataExtent

    spaceM = Math.min(spaceX, spaceY)
    radius = ((spaceM / 2)  * radiusPercent) - (spokeLabelSpacer+spokeExtension)*2
    angleR = Math.PI * 2 / axes.length

    scaleR.domain([0, dataMax]).range([0, radius])

    let gCenter = container.select(`g.${n('center')}`)
    if (gCenter.empty()) gCenter = container.append('g').attr('class', `${n('center')}`)
    gCenter.attr('transform', `translate(
      ${(spaceX-(spokeLabelSpacer+spokeExtension*2))/2},
      ${(spaceY-(spokeLabelSpacer+spokeExtension*2))/2})`
    )



    let defs = gCenter.select(`defs`)
    if (defs.empty()) defs = gCenter.append('defs')
    let filt = defs.select(`filter#${n('glow')}`)
    if (filt.empty()) {
      filt = defs.append('filter').attr('id', `${n('glow')}`)
      let feGaussianBlur = filt.append('feGaussianBlur').attr('stdDeviation','2.5').attr('result','coloredBlur'),
  		feMerge = filt.append('feMerge'),
  		feMergeNode_1 = feMerge.append('feMergeNode').attr('in','coloredBlur'),
  		feMergeNode_2 = feMerge.append('feMergeNode').attr('in','SourceGraphic');
    }



    let gAxesCon = gCenter.select(`g.${n('axes-container')}`)
    if (gAxesCon.empty()) gAxesCon = gCenter.append('g').attr('class', `${n('axes-container')}`)


    // concentric numberOfLevels
    axesLevels = gAxesCon.selectAll(`.${n('axis-level')}`)
      .data(d3.range(1, numberOfLevels+1).reverse())
    axesLevels.exit().remove()
    axesLevels = axesLevels.merge(
      axesLevels.enter().append('circle').attr('class', `${n('axis-level')}`)
    )
    axesLevels
      .transition(transitionDuration)
      .ease(easeFn)
      .attr('r', (d,i)=>radius/numberOfLevels*d)
      .style('fill', levelFill)
      .style('stroke', levelStroke)
      .attr('fill-opacity', levelOpacity)
    if (levelBlur) axesLevels.style('filter', `url(#${n('glow')})`)

    // values of concentric numberOfLevels
    axesLabels = gAxesCon.selectAll(`.${n('axis-level-label')}`)
      .data(d3.range(1, numberOfLevels+1).reverse())
    axesLabels.exit().remove()
    axesLabels = axesLabels.merge(
      axesLabels.enter().append('text').attr('class', `${n('axis-level-label')}`)
    )

    axesLabels
      .transition(transitionDuration)
      .ease(easeFn)
      .attr('x', 4)
      .attr('y', d=>-d*radius/numberOfLevels)
      .attr('dy', '0.4em')
      .style('font-size', levelLabelFontSize)
      .attr('fill', levelLabelFill)
      .text((d,i)=>dataMax*d/numberOfLevels)




    // spokes
    axesSpokes = gAxesCon.selectAll(`g.${n('axis-spoke')}`).data(axes)
    axesSpokes.exit().remove()
    axesSpokes = axesSpokes.merge(
      axesSpokes.enter().append('g').attr('class', `${n('axis-spoke')}`)
    )

    spokeLines = axesSpokes.select(`line.${n('axis-line')}`)
    if (spokeLines.empty()) spokeLines = axesSpokes.append('line').attr('class', `${n('axis-line')}`)


    spokeLines
      .transition(transitionDuration)
      .ease(easeFn)
  		.attr('x1', 0)
  		.attr('y1', 0)
  		.attr('x2', (d, i)=>(scaleR(dataMax)+spokeExtension) * Math.cos(angleR*i - Math.PI/2))
  		.attr('y2', (d, i)=>(scaleR(dataMax)+spokeExtension) * Math.sin(angleR*i - Math.PI/2))
  		.style('stroke', spokeStroke)
  		.style('stroke-width', spokeStrokeWidth);


    spokeLabels = axesSpokes.select(`text.${n('spoke-label')}`)
    if (spokeLabels.empty()) spokeLabels = axesSpokes.append('text').attr('class', `${n('spoke-label')}`)
    spokeLabels
      .transition(transitionDuration)
      .ease(easeFn)
  		.style('font-size', spokeFontSize)
  		.attr('text-anchor', 'middle')
  		.attr('dy', '0.35em')
  		.attr('x', (d, i)=> (scaleR(dataMax)+spokeExtension+spokeLabelSpacer) * Math.cos(angleR*i - Math.PI/2))
  		.attr('y', (d, i)=> (scaleR(dataMax)+spokeExtension+spokeLabelSpacer) * Math.sin(angleR*i - Math.PI/2))
  		.text(d=>d)
  		.call(wrapText, wrapWidth);

    spokeLabels
      .on('mouseover', spokeLabelMouseover)
      .on('mouseenter', spokeLabelMouseover)
      .on('mouseleave', spokeLabelMouseleave)
      .on('mouseexit', spokeLabelMouseleave)

    spiderLine = d3.lineRadial()
      .radius((d)=>scaleR(d))
      .angle((d, i)=>angleR*i)
      .curve(curve)



    let webContainer = gCenter.select(`g.${n('web-container')}`)
    if (webContainer.empty()) webContainer = gCenter.append('g').attr('class', `${n('web-container')}`)

    webs = webContainer.selectAll(`g.${n('web-wrapper')}`).data(dataKeys)
    webs.exit().remove()
    webs = webs.merge(
      webs.enter().append('g').attr('class', `${n('web-wrapper')}`)
    )


    webOutlines = webs.select(`path.${n('web-outline')}`)
    if (webOutlines.empty()) webOutlines = webs.append('path').attr('class', `${n('web-outline')}`)
    webOutlines
      .transition(transitionDuration)
      .ease(easeFn)
      .attr('d', function(d, i){
        return spiderLine(dataValues[i])
      })
      .attr('stroke', (d,i)=>colorExtractor(d, i))
      .attr('stroke-width', webStrokeWidth)
      .style('fill', 'none')



    webFills = webs.select(`path.${n('web-fill')}`)
    if (webFills.empty()) webFills = webs.append('path').attr('class', `${n('web-fill')}`)
    webFills
      .transition(transitionDuration)
      .ease(easeFn)
      .attr('d', function(d, i){ return spiderLine(dataValues[i]) })
      .attr('fill', (d,i)=>colorExtractor(d, i))
      .attr('opacity', webOpacity)

    webFills
      .on('mouseover', webFillMouseover)
      .on('mouseenter', webFillMouseover)
      .on('mouseleave', webFillMouseleave)
      .on('mouseexit', webFillMouseleave)



    webPointsContainer = webs.select(`g.${n('web-point-con')}`)
    if (webPointsContainer.empty()) webPointsContainer = webs.append('g').attr('class', `${n('web-point-con')}`)

    webPoints = webPointsContainer.selectAll(`circle.${n('web-point')}`).data((d,i)=>{
      return dataValues[i].map((v,j)=>({v,d,a:axes[j]}))
    })
    webPoints.exit().remove()
    webPoints = webPoints.merge(
      webPoints.enter().append('circle').attr('class', `${n('web-point')}`)
    )
    webPoints
      .transition(transitionDuration)
      .ease(easeFn)
      .attr('r', webPointRadius)
  		.attr('cx', (d,i)=>scaleR(d.v) * Math.cos(angleR*i - Math.PI/2))
  		.attr('cy', (d,i)=>scaleR(d.v) * Math.sin(angleR*i - Math.PI/2))
  		.style('fill', (d,i,j)=>{
        return colorExtractor(d.d, dataKeys.indexOf(d.d))
      })
		    .style('fill-opacity', webPointOpacity);
    webPoints
      .on('mouseover', webPointMouseover)
      .on('mouseenter', webPointMouseover)
      .on('mouseleave', webPointMouseleave)
      .on('mouseexit', webPointMouseleave)
  }


  return spider
}
