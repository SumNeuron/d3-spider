import * as d3 from 'd3'

export function getTranslation(transform) {
  var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
  transform = transform == undefined ? 'translate(0,0)' : transform;
  g.setAttributeNS(null, 'transform', transform);
  var matrix = g.transform.baseVal.consolidate().matrix;
  return [matrix.e, matrix.f];
}

export function resizeDebounce(f, wait) {
  var resize = debounce(function(){f()},wait)
  window.addEventListener('resize', resize)
}


export function debounce(func, wait, immediate) {
  var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}


export function wrapText(text, width) {
	  text.each(function() {
  		var text = d3.select(this),
  			words = text.text().split(/\s+/).reverse(),
  			word,
  			line = [],
  			lineNumber = 0,
  			lineHeight = 1.4, // ems
  			y = text.attr("y"),
  			x = text.attr("x"),
  			dy = parseFloat(text.attr("dy")),
  			tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");

  		while (word = words.pop()) {
  		  line.push(word);
  		  tspan.text(line.join(" "));
  		  if (tspan.node().getComputedTextLength() > width) {
  			line.pop();
  			tspan.text(line.join(" "));
  			line = [word];
  			tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
  		  }
  		}
	  });
	}
